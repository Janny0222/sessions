//use 'require' directive to load the Node.js modules
//a module is a software component or part of a program that contains one or more routines
//the "http" module lets node.js transfer data using the hyper text transfer protocol(http)
// http is a protocol that allows the fetching of resources like html documents
let http = require("http");

// createServer() method creates an HTTP server that listens to requests on a specific port and gives response back to the client
// the argument passes in the function are request and respone object (data type) that containes method that allow us to received request from the client and send responses back to it
// 4000 is the port where the server will listen to. A port is a virtual point where the network connections start and end
// function(clientRequest, serverRequest)
http.createServer(function(request, response){

    //200 - status code of the response - means OK
    //sets the content-type of the response to be a plain text message
    response.writeHead(200, {'Content-Type': 'text/plain'})


    //send the response with the text content 'Hello World'
    response.end('Hellow World');

}).listen(4000)

// when server is running, console will print the message
console.log('Server is running at port 4000')
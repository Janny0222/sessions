const jwt = require('jsonwebtoken');
const secret_key = "CourseBookingAPI303";

// Generating a token
module.exports.createAccessToken = (user) => {
	const user_data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	// The jwt.sign() will generate a token using the user data and secret key. The 3rd argument serves as additional options for the token
	return jwt.sign(user_data, secret_key, {});
}

// Verifying a token
module.exports.verify = (request, response, next) => {
let token = request.headers.authorization;

if(typeof token === "undefined"){
	return response.send({auth: 'Failed, please include the token in the header of the request.'})
	}
	//console.log(token);

	// This will remove the default 'Bearer' from the authorization header, leaving only the token itself
	token = token.slice(7, token.length);

	// console.log(token)

	jwt.verify(token, secret_key, (error, decoded_token) => {
		if(error){
			return response.send({
				auth: 'Failed', 
				message: error.message
			})
		}
		//console.log(decoded_token)
		request.user = decoded_token; // set the value of the result.user to the decoded token which contains the user data
		next(); // -> controller for JWT
	})
}

// verifying if user is admin
module.exports.verifyAdmin = (request, response, next) =>{
	// check the request.user object which come from the 'verify' function and contains
	if(request.user.isAdmin){
	return	next();
	}
	return response.send({
		auth: 'Failed',
		message: 'Action forbidden'
	})
}
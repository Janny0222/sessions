const express = require('express');
const router = express.Router();
const CourseController = require('../controllers/CourseController.js')
const auth = require('../auth.js')

// You can destructure the 'auth' variable to extract the function being exported from it. You can then use the function directly without having to use dot (.) notation
//const {verify, verifyAdmin} = auth


// Create single course
router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.addCourse(request, response)
})


// Get all course

router.get('/all', (request, response) => {
	CourseController.getAllCourses(request, response);
})

// Get all active Courses

router.get('/', (request, response) => {
	CourseController.getAllActiveCourses(request, response);
})

// Get single course course
router.get('/:id', (request, response) => {
	CourseController.getSingleCourse(request, response);
})

// Update course
router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.updateCourse(request, response);
})
// Archive single course
router.put('/:id/archive', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.archiveCourse(request, response);
})
// Activate single course
router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.activateCourse(request, response);
})
// Search course by name
router.post('/search', (request,response) => {
	CourseController.searchCourses(request, response)
})

router.delete('/:id/remove', auth.verify, auth.verifyAdmin, (request, response) => {
  CourseController.deleteCourse(request, response);
});

module.exports = router;
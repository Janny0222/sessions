// Server Variable
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
require('dotenv').config();
const app = express();
const port = 4000;
const userRoutes = require('./routes/userRoutes.js')
const courseRoutes = require('./routes/courseRoutes.js')

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors())

// Routes
app.use('/api/users', userRoutes);
app.use('/api/courses', courseRoutes);
//app.use('/api/courses', courseRoutes);

// Database Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@b303-villaruel.6drhyd4.mongodb.net/b303-booking-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', () => console.log("Can't connect to database"));
db.once('open', () => console.log('Connected to MongoDB'));


// Since we're hosing this API in the cloud, the port to be used should be flexible hence, the use of the process.env.PORT which will take the part that the cloud server uses if the 'port' variable isn't available.
app.listen(process.env.PORT || port, () => {
	console.log(`Booking System API is now running at localhost:${process.env.PORT || port}`);
})

module.exports = app;
const Course = require('../models/Course.js');
const auth = require('../auth.js')
const mongoose = require('mongoose')
// Add Course
module.exports.addCourse = (request, response) => {
	let new_course = new Course ({
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	});
	return new_course.save().then((saved_course, error) => {
		if (error){
			return response.send(false)
		}
		return response.send(true);

	})
	.catch(error => console.log(error));
}
// Getting All Courses
module.exports.getAllCourses = (request, response) => {
	return Course.find({}).then(result => {
		return response.send(result);
	})
}
// Getting All Courses Active
module.exports.getAllActiveCourses = (request, response) => {
	return Course.find({isActive: true}).then(result => {
		return response.send(result)
	})
}
// Getting Single Course
module.exports.getSingleCourse = (request, response) => {
	return Course.findById(request.params.id).then(result => {
		return response.send(result)
	})
}
// Updating Course
module.exports.updateCourse = (request, response) => {
	let updated_course_details = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	};
	return Course.findByIdAndUpdate(request.params.id, updated_course_details).then((course, error) => {
		if(error){
			return response.send({ message: error.message })
		}
		return response.send({message: 'Course has been updated successfully!'})
	})
}
// Archiving the Course
module.exports.archiveCourse = (request, response) => {
	
	return Course.findByIdAndUpdate(request.params.id, {isActive: false}).then((result, error) => {
		if(error){
			return response.send({message: error.message})
			}
			return response.send({message: "Course successfully archived"});
		}) 
}
// Activating the Course
module.exports.activateCourse = (request, response) => {
	return Course.findByIdAndUpdate(request.params.id, {isActive: true}).then((result, error) => {
	
		if(error){
			return response.send({message: error.message})
			}
			return response.send({message: "Course successfully activate"});
		}) 
}

module.exports.searchCourses = (request, response) => {
      const courseName  = request.body.courseName;
      return Course.find({ name: { $regex: courseName, $options: 'i' } }).then((courses)=> {
      	response.send(courses)
      }).catch(error => response.send({message: error.message}))
    }
  

module.exports.deleteCourse = (request, response) => {
  const courseId = request.params.id;
 // Check if the courseId is a valid ObjectId
  if (!mongoose.Types.ObjectId.isValid(courseId)) {
    return response.status(400).json({ message: 'Invalid course ID' });
  }

  Course.findByIdAndRemove(courseId).then((result, error) => {
      if (error) {
        return response.status(500).json({ message: 'Failed to delete the course' });
      }

      if (!result) {
        return response.status(404).json({ message: 'Course not found' });
      }

      return response.status(200).json({ message: 'Course deleted successfully' });
    })
    .catch((error) => {
      response.status(500).json({ message: 'Internal server error' });
    });
};
// Server Variables
const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config() // Initialization of dotenv package
const app = express();
const port = 4000;

// MongoDB Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@b303-villaruel.6drhyd4.mongodb.net/b303-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', () => console.log('Connection error :('));
db.once('open', () => console.log('Connected to MongoDB'));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// [SECTION] Mongoose Schema

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// [SECTION] Models
const Task = mongoose.model("Task", taskSchema);

// [SECTION] Routes
app.post('/tasks', (request, response) => {
	Task.findOne({name: request.body.name}).then((result, error) => {
		// Check if the task already exist by utilizing the 'name' property. If it does, then return a response to the user.
		if(result != null && result.name == request.body.name){

			return response.send("Duplicate task found!");
		} else {

			// 1. create a new instance of the task model which will contain that properties required based on the schema
			let newTask = new Task ({
				name: request.body.name
			});

			// 2. save the new task to the database
			newTask.save().then((savedTask, error) => {
				if(error){
					return response.send({
						message: error.message
					});
				}

				return response.send(201, 'New user created!');
			})
		}
	})
})

app.get('/tasks', (request, response) => {
	Task.find({}).then((result, error) => {
		if(error){
			return response.send({
				message: error.message
			})
		}
		return response.status(200).json({
			tasks: result
		})
	})
})

// app.delete('/tasks', (request, response) => {
// 	Task.deleteMany({}).then((result, error) => {
// 		if (error){
// 			return response.send({
// 				message: error.message
// 			})
// 		}
// 		return response.json('All data deleted successfully');
// 	})
// })

app.delete('/tasks', (request, response) => {
	//let {name} = request.body;
	Task.deleteOne({name: request.body.name}).then((result, error) => {
		if(error){
			return response.status(404).json({error: 'Data Not Found'});
		}
		// if (request.body.name !== result.name) {
		// 	return response.json({message: 'Task not found, please try again'})
		// }
		return response.send(201, `The task ${request.body.name} has been deleted Successfully`);
	})
	.catch((error) => {
		return response.status(500).json({error: 'Error deleting data'});
	})
})

// Server Listening
app.listen(port, () => console.log(`Server is running at localhost:${port}`));

module.exports = app;
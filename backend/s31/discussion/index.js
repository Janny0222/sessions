// JSON Format Example
// {
// 	"city": "Pateros",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// [SECTION] JSON Arrays
// "cities": [
// 	{ 
// 		"city": "Quezon City",
// 		"province": "Metro Manila",
// 		"country": "Philippines"
// 	},
// 	{ 
// 		"city": "Batangas City",
// 		"province": "Batangas",
// 		"country": "Philippines"
// 	},
// 	{ 
// 		"city": "Star City",
// 		"province": "Pasay",
// 		"country": "Philippines",
// 		"rides":[
// 			{ 
// 				"name": "Star Flyer",
// 			},
// 			{
// 				"name": "Gabi ng Lagim"
// 			}

// 		]
// 	}
// ]

// [SECTION] JSON Methods
let zuitt_batches = [
	{ batchName: "303" },
	{ batchName: "271" }
]
console.log(`Output before stringification:`);
console.log(zuitt_batches)

// After the JSON.stringify function, javascript now reads the variable as a string(equivalent to converting the array into JSON)
console.log("Output after stringification:")
console.log(JSON.stringify(zuitt_batches))

// User details
let first_name = prompt("What is your First Name?");
let last_name = prompt("What is your Last Name?");

// The Stringify function/method converts JS Object/Array into JSON Strings
let other_data = JSON.stringify({
	firstName: first_name,
	lastName: last_name
})
console.log(other_data)

// [SECTION] Convert stringified JSON into Javascript Objects/Arrays
let other_data_JSON = `[{"firstName": "Janny", "lastName": "Villaruel"}]`;

// The parse function/method converts the JSON Strings into a JS Object/Array
let parsed_other_data = JSON.parse(other_data_JSON);

console.log(parsed_other_data);
console.log("Result of the JSON.parse")
console.log(parsed_other_data[0].firstName);
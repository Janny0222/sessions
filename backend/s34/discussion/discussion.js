// CRUD Operations
/*
	- CRUD means Create, Retrieve, Update, and Delete.
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// [Section] Inserting documents (Create)

// Insert one document
/*
    - Since mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
    - The mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code
    - Creating MongoDB syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly in the terminal where the whole code is only visible in one line.
    - By using a text editor it allows us to type the syntax using multiple lines and simply copying and pasting the code in terminal will make it work.
    - Syntax
        - db.collectionName.insertOne({object});
    - JavaScript syntax comparison
        - object.object.method({object});
*/
db.users.insertOne({
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    contact: {
        phone: "0912345678",
        email: "janedoe@gmail.com"
    },
    courses: [ "CSS", "Javascript", "Python" ],
    department: "none"
});

// Inserting multiple documents at once
db.users.insertMany([
    {
        "firstName": "John",
        "lastName": "Doe",
    },
    {
        "firstName": "Joseph",
        "lastName": "Doe"
    }
]);

// [SECTION] RETRIEVING DOCUMENTS
// Retrieving all the insreted users
db.users.find();

// Retrieving specific document
db.users.find({"firstName": "John"});

// [SECTION] Updating existing documents
db.users.updateOne(
{
    "_id": ObjectId("64c0e12b4c4a13471d125e03")
},
{
    $set: {
        "firstName": "Jane"
    }
}
);

// For updating multiple documents
db.users.updateMany(
{
    "lastName": "Gaza"
},
{
    $set: {
        "firstName": "Mary"
    }
}
);
// For deleting multiple
db.users.deleteMany({"lastName": "Gaza"})

// Deleting single document
db.user.deleteOne({
    "_id": ObjectId("")
})

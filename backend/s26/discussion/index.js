//[SECTION] Arrays and Indexes

let grade = [98.5, 94.3, 89.2, 90.1];
let computer_brands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu", "Lenovo"];
let mixed_array = [12, "ASUS", null, undefined, {}];

// Alternative way to write arrays
let my_task = [
	"drink html",
	"eat javascript",
	"inhale CSS",
	"bake sass"
];

// Re-asigning Values

console.log("Array before re-assignment");
console.log(my_task);

//to re-assign a value in an array, just use its index number and use an assigment operator to replace the value of that index
my_task[0] = "run hello world"
console.log("Array after re-assignment");
console.log(my_task);

// [SECTION] Reading from Arrays
console.log(computer_brands[1]);
console.log(grade[3]);


// Getting the lenght of an array
console.log(computer_brands.length);

// Accessing last element in an array

let index_of_last_element = computer_brands.length - 1;

console.log(computer_brands[index_of_last_element]);

// [SECTION] Array Methods

// Push Method - Adds a new item to the of the array
let fruits = ["Apple", "Orange", "Kiwi", "Passion Fruit"];
console.log("Current Array:")
console.log(fruits);

fruits.push("Mango", "Cocomelon");

console.log("Updated array after push method:");
console.log(fruits)

// Pop Method - removes the last item and you can assign it to a variable to access the specific item that was removed
console.log("Current Array:")
console.log(fruits);

let removed_item = fruits.pop();

console.log("Updated array after pop method:");
console.log(fruits)
console.log("Removed fruit: " + removed_item);

// Unshift methodd - Adding items in the beginning of the array
console.log("Current Array:")
console.log(fruits);

fruits.unshift("Lime", "Star Apple");

console.log("Updated array after unshift method:");
console.log(fruits)

// Shift Method - Remove items at the beginning of array
console.log("Current Array:")
console.log(fruits);

fruits.shift();

console.log("Updated array after shift method:");
console.log(fruits)

// Splice Method
console.log("Current Array:")
console.log(fruits);

// splice(startingIndex, numberOfItemsToBeDeleted, itemsToBeAdded)
fruits.splice(1, 2, 'Lime', 'Cherry');

console.log("Updated array after splice method:");
console.log(fruits)

// Sort Method - Can sort items in an array alphanumerically either in ascending order or descending
console.log("Current Array:")
console.log(fruits);

fruits.sort();

console.log("Updated array after sort method:");
console.log(fruits)

fruits.reverse();
console.log("Updated array after reverse method:");
console.log(fruits)

// [Sub-section] Non-Mutator Methods - Every method written above is calleds a 'Mutator Method' because it modifies the value of the array one way or another. Non-mutator methods on the other hand, don't do the same thing, they instead execute the specific functionalities that can be done with the existing array values.


// indexOf method - gets the index of a specific item.
let index_of_lenovo = computer_brands.indexOf("Lenovo");
console.log("The index of Lenovo is: " + index_of_lenovo);

let index_of_lenovo_from_last_item = computer_brands.lastIndexOf("Lenovo");
console.log("The index of lenovo starting from the end of an array is: " + index_of_lenovo_from_last_item);

// Slice Method
console.log("")
let hobbies = ["Gaming", "Running", "Gaslighting", "Cycling", "Writing"];

let sliced_array_from_hobbies = hobbies.slice(2);
console.log("slice method output using index number")
console.log(sliced_array_from_hobbies);
console.log(hobbies)

let sliced_array_from_hobbies_b = hobbies.slice(2, 4);
console.log(sliced_array_from_hobbies_b);
console.log(hobbies)

let sliced_array_from_hobbies_c = hobbies.slice(2);
console.log(sliced_array_from_hobbies_c);
console.log(hobbies)

// toString Method
console.log("")
let string_array = hobbies.toString();
console.log("toString Method Output:")
console.log(string_array)

// concat Method
console.log("")
let greeting = ["Hello", "World"];
let exclamation = ["!", "?"];

let concat_greeting = greeting.concat(exclamation);
console.log("concat Method Output:")
console.log(concat_greeting);

// join method - this will convert the array to string and insert a specified seperator between them. The seperator is define as the argument of the join() function
console.log("");
console.log("join Method Output:");
console.log(hobbies.join(' || '));

// forEach Method
console.log("");
console.log("forEach Method Output:");
hobbies.forEach(function(hobby){
	console.log(hobby);
})

// map Method - Loobs through the whole array and adds each item to a new array.
console.log("");
let number_list = [1, 2, 3, 4, 5];

let numbers_map = number_list.map(function(number){
	return number * 2;
});
console.log("map Method Output:");
console.log(numbers_map);

// filter Method
console.log("");
let filtered_numbers = number_list.filter(function(number){
	return (number < 3);
})
console.log("filter Method Output:")
console.log(filtered_numbers);

// [SECTION] Multi-Dimensional Arrays
console.log("");
let chess_board = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
	['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
	['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
	['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];
console.log("Multi-Dimensional Output:")
console.log(chess_board[3][2]);
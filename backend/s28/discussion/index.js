
// [SECTION] Iteration Methods -> loops through all the elements to perform repatitive task on the array

// forEach() - to loop through the array
// map() - loops through the array and return a new array
// filter() - returns a new array containing elements which meets the given condition

// every() - checks if all elements meet the given condition
// return true of all elements meet the given condition, however, false if its does not
let numbers = [1, 2, 3, 4, 5, 6, 7, 8];

let allValid = numbers.every(function(number) {
	return number > 3;
})
console.log("Result of every() method: ")
console.log(allValid)

// some() - check if atleast one element meets the given condition
// returns true if atleast one element meets the given condition, return false other wise
let someValid = numbers.some(function(number){
	return number < 2;
})
console.log("Result of some() method: ")
console.log(someValid)

// includes() method - method can be 'chained' using then one after another
// return the value of what word or letter inside the include()
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a')
})
console.log(filteredProducts)

// reduce()
let iteration = 0;
let reduceArray = numbers.reduce(function(x, y){
	console.warn("Current iteration: " + ++iteration);
	console.log("accumulator: " + x);
	console.log("CurrentValue: " + y);
	return x + y;
})
console.log("Result of reduce method: " + reduceArray);

let productsReduce = products.reduce(function(x, y){
	return x + y;
})

console.log("Result of reduce() method: " + productsReduce)
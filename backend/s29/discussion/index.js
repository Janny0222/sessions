// console.log("hello world!")

// Get post data - to retrieve data from jsonplaceholder API.

// fetch('https://jsonplaceholder.typicode.com/posts').then(function(response) { console.log(response); }); -> other way to create function

//Promise chain
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => { 
	showPosts(data)
});

console.log(document)
console.log(document.querySelector('#form-add-post'));
let txtTitle = document.querySelector('#txt-title').value;
console.log(txtTitle);

// Add Post Data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	// prevent default behavior of our form element
	e.preventDefault()
	// function code block or tak after triggering the event
	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userID: 1
		}),
		headers: {
			'Content-type': 'application/json'
		}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Post Successfully created');

		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})
})
// Show post - to display all the posts object from our jsonplaceholder - API.

const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		// JSX Syntax - Javascript Extended/Extension
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete</button>
			</div>

		`;
	});

	document.querySelector('#div-post-entries').innerHTML = postEntries;
};

// Edit Post form
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
}

//Update button function / Update post

document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userID: 1
		}),
		headers: {
			'Content-type': 'application/json'
		}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert("Post update Successfully");

		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;
		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	})
});


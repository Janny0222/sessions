//[SECTION] While Loop

// let count = 5;

// while (count !== 0){
// 	console.log("Current Value of While Loop: " + count);
// 	count--;
// }

// //[SECTION] Do-While Loop

// let number = Number(prompt("Give me a Number"));

// do{
// 	console.log("Current Value of Do-While Loop: " + number);
// 	number += 1;
// } while (number < 10)

// //[SECTION] For Loop

// for(let count = 0; count <=20; count++){
// 	console.log("Current Value of For Loop: " + count);
// }

// let my_string = "jan michael";
// // to get the length of a string
// console.log(my_string.length);

// // to get a specific letter in a string
// console.log(my_string[3]);

// // Loops through each letter in the string and will keep iterating as long as current index is less than the length of the string
// for(let index = 0; index < my_string.length; index++){
// 	console.log(my_string[index]);
// }

let my_string ="jan michael villaruel";
let index2 = my_string.replace(/[aeiou]/gi, '');
// let my_string_consonant_only = my_string.replace(/[bcdfghjklmnpqrstvwxyz]/gi, '');
for(let index = 0; index < index2.length; index++){
	console.log(index2[index]);

}
// console.log("")
// for (let index = 0; index < my_string_consonant_only.length; index++){
// 	console.log(my_string_consonant_only[index]);
// }
console.log("");
let name_two = "rafael";
for(let index = 0; index < name_two.length; index++){
	// console.log(name_two[index]);

	if(name_two[index].toLowerCase() == "a"){
		console.log("Skipping...");
		continue;
	}
	if(name_two[index] == "e"){
		break;
	}
	console.log(name_two[index]);
}

console.log("");

let full_name = "kimberly del castillo";
for(let index = 0; index < full_name.length; index++){
	if(full_name[index].toLowerCase() != 'a' && full_name[index].toLowerCase() != 'e' && full_name[index].toLowerCase() != 'i' && full_name[index].toLowerCase() != 'o' && full_name[index].toLowerCase() != 'u')
		console.log(full_name[index]);

}

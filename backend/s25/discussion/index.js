// console.log("love, objects");

// an object os a data type that usedd tp represent real word objects and create properties and methods/functionalities
// [SECTION] Objects

// creating object using initializers/object literals
let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
}

console.log("Result from creating object using initializers/object literals");
console.log(cellphone);
console.log(typeof cellphone);

// creating object using constructor function
function Laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
	// body...
}

// multiple instance of an object using the 'new' keyword
// this method is called instantiation
let laptop = new Laptop('Lenovo', 2008)
console.log("Result from creating objects using constructor function")
console.log(laptop);

let laptop2 = new Laptop('MacBook Air', 2022);
console.log("Result from creating objects using constructor function")
console.log(laptop2);

// [SECTION] Accessing Object Properties

// using square bracket notation and double quote of the property name -> variable["property-name"]
console.log("Result from square bracket notation: " + laptop2["name"]);

// using dot notation -> variable.property-name
console.log("Result from dot notation: " + laptop2.name);

// access array objects
let array = [laptop, laptop2];

// using variable-array[index of variable-array]["property"]
console.log(array[0]['name']);

// using variable-array[index of variable-array].property]
console.log(array[0].name);

// [SECTION] Adding/Deleting/Reassigning Object Properties
// Empty object -> you can use if you have blank data or values
let car = {};

// Empty object using constructor function/instation
let myCar = new Object();

// adding object properties
// object-name.key-name = 'value'
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation: ");
console.log(car);

// adding object properties using square bracket notation
// object["property-name"] = "value"
car["manufacturing date"] = 2019;
console.log(car["manufacturing date"])
console.log(car["Manufacturing Date"])

// cannot access the object property using dot notation of the key has spaces
// console.log(car.manufacturing date);
console.log('Result from adding properties using square bracket notation');
console.log(car);

// deleting object properties using square bracket notation -> delete object["property"]
delete car['manufacturing date'];

// deleteing object properties using dot notation -> delete object.property
console.log('Result from deleting propreties: ')
console.log(car)

// reassigning object properties using dot notation object.property = "new value"
car.name = 'Honda Civic Type R';
console.log('Result from reassigning properties: ');
console.log(car)

// [SECTION] Object Methods - a method is a function witch acts as a propertyy of an object
// let variable = {key: 'value', key: function(){console.log("Message" + this.property)}}
// console.log(object); console.log("message"); object.key();
let person = {
	name: 'Barbie',
	greet: function(){
		console.log("Hello! My name is " + this.name);
	}
}
console.log(person)
// console.log('Result form object methods: ');
person.greet();

// adding methods to object -> object.name-of-function = function(){console.log(this.key-name + " message")} object.name-of-function();
person.walk = function() {
	console.log(this.name + " walked 25 steps forward")
}
person.walk();


// combine object with object -> let variable = {key-name: value, key-name{key-name: value, key-name: value} object.name-of-function = function(object){console.log(this.key-name + " message")} object.name-of-function();
let friend = {
	name: 'Ken',
	address: {
		city: "Austin",
		state: "Texax",
		country: "USA"
	},
	email: ['ken@gmail.com', 'ken@mail.com'],
	introduce: function(person){
		console.log('Nice to meet you ' + person.name + " I am " + this.name + ' from ' + this.address.city + '' + this.address.state);
	}
}
friend.introduce(person);
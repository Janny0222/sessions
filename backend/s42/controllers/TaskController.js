const Task = require('../models/Task.js')
const mongoose = require('mongoose');

module.exports.getAllTasks = () => {
	return Task.find({}).then((result, error) => {
		if(error){
			return {
				message: error.message
			}
		}
		return {
			tasks: result
		}
	})
}

module.exports.createTask = (request_body) => {
	return Task.findOne({name: request_body.name}).then((result, error) => {
		if(result != null && result.name == request_body.name){
			return { message: "Duplicate task found!" }
		} else {
			let newTask = new Task ({
				name: request_body.name
			});
			return newTask.save().then((savedTask, error) => {
				if(error){
					return {
						message: error.message
					};
				}
				return { 
					message: `Task ${request_body.name} has been created!` 
				};
				
			})
		}
	})
}

module.exports.deleteTask = (requestBody) => {
	return Task.findOneAndDelete({name: requestBody.name}).then((result, error) => {
		if(!result){
			return { message: `${requestBody.name} was not found in database`}
		}
		if(error){
			return response.status(404).json({error: 'Data Not Found'});
		}
		return {
			message: `The task ${requestBody.name} has been deleted Successfully` 
		};
	})
	.catch((error) => {
		return response.status(500).json({error: 'Error deleting data'});
	})
}

module.exports.getSpecificTasks = (request_params_id) => {
	return Task.findOne({_id: request_params_id}).then((result, error) => {
		if(error){
			return{
				message: error.message
			}
		}
		return result;
	})
}
// Server Variables
const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config() // Initialization of dotenv package
const taskRoutes = require('./routes/taskRoutes.js')
const app = express();
const port = 4000;

// MongoDB Connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@b303-villaruel.6drhyd4.mongodb.net/b303-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', () => console.log('Connection error :('));
db.once('open', () => console.log('Connected to MongoDB'));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/api/tasks', taskRoutes); // Initializing the routes for '/tasks so the server will know the routes available to send request to'


// Server Listening
app.listen(port, () => console.log(`Server is running at localhost:${port}`));

module.exports = app;
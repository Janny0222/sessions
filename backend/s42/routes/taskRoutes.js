const express = require('express');
const router = express.Router();
const TaskController = require('../controllers/TaskController.js');

// Insert routes here
// Creating a new Task
router.post('/', (request, response) => {
	TaskController.createTask(request.body).then(result => {
		response.send(result);
	})
})

//Get All Task
router.get('/', (request, response) => {
	TaskController.getAllTasks().then(result => {
		response.send(result)
	})
})

router.get('/:id', (request, response) => {
	TaskController.getSpecificTask(request.params.id).then(result => {
		response.send(result)
	})
})

// Delete Specific Task
router.delete('/', (request, response) => {
	TaskController.deleteTask(request.body).then(result => {
		response.send(result)
	})
})

module.exports = router;
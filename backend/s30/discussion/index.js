console.log("ES6 Update!")

// Exponent Operator
const firstNum = 8 ** 2;
console.log(firstNum);

// Math operator
const secondNum = Math.pow(8, 2);
console.log(secondNum);

// Template Literal 
// allow us to write string w/o using concatenation
// greatly helpds us with code readability

let name = "Ken";

// using concatenation
let message = 'Hello ' + name + '! Welcome to programming';
console.log('Messsage without template literals: ' + message);

// using template literals
// backticks (``)
message = `Hello ${name}! Welcome to Programming`;
console.log(`Message with template literals: ${message}`);

// create multi-line using template literals
const anotherMessage = `
${name} attended Math competition. He won it by solving the problem 8 ** 2 with the answer of ${firstNum}`;
	console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings amount is: ${principal * interestRate}`);

//[SECTION] Array Destructuring

const fullName = ["Juan", "Dela", "Cruz"];

// using array indexes
console.log(`Using array indexes`)
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

// using array destructuring

const [firstName, middleName, lastName] = fullName;
console.log(`Using array destructuring`)
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);


//[SECTION] Object Destructuring
const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
}

// using dot notation
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.lastName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you`);

// using object destructuring
const {givenName, familyName, maidenName} = person;
console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you`)

// using object destructuring in functions
function getFullName({givenName, maidenName, familyName}) {
console.log(`${givenName} ${maidenName} ${familyName}`)
};
getFullName(person);

//[SECTION] Arrow functions
const hello = () => {
	console.log("Hello World!")
}
hello();

// Traditional functions without Template Literals
function printJohnName(fName, mName, lName){
	console.log(fName + ' ' + mName + ' ' + lName);
}
printJohnName('John', 'D', 'Smith');


// Arrow Function with Template Literals
const printFullName = (fName, mName, lName) => {
	console.log(`${fName} ${mName} ${lName}`)
}
printFullName('Jane', 'D', 'Smith')

//[SECTION] Arrow functions with Loops
const students = ['John', 'Jane', 'June']

// Sample of Traditional Function Loops
console.log("Using Traditonal Function Loops")
students.forEach(function(student){
	console.log(`${student} is a student.`)
})

// Arrow Function Loops
console.log("Using Arrow Function Loops")
 students.forEach((student)=> {
 	console.log(`${student} is a student.`)
 })

 //[SECTION] Implicit Return Statements

 // Traditional Function
 // function add(x, y){
 // 	return x + y;
 // }
 // let total = add(2, 5);
 // console.log(total);

 // Arrow function
 // single line arrow function
 const add = (x, y) => x + y;

 let total = add(4, 5);
 console.log(total);

 // [Default] Argument values
 // Provides a default argument value if none is provided when the function call
 const greet = (name = 'User') => {
 	return `Good afternoon ${name}`
 }
 console.log(greet()); // Good Afternoon User
 console.log(greet('Judy')); //Good afternoon Judy

// [SECTION] Class-based Object Blueprints

 // Create a clas
 class Car {
    constructor(brand, name, year){
        this.brand = brand;
        this.name = name;
        this.year = year;
    }
 }

 // Initiate an object
const fordExplorer = new Car();

// Even though the 'fordExplorer' object is const, since it it an object, you may still re-assign the values to its properties.
fordExplorer.brand = "Ford";
fordExplorer.name = "Explorer";
fordExplorer.year = 2022;

console.log(fordExplorer);

// This logic applies wether you re-assign the values of each property separtely or put it as argument of the new instance of the class
const toyotaVios = new Car("Toyota", "Vios", 2018)

console.log(toyotaVios);
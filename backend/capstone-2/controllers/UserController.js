const User = require('../models/User.js');
const auth = require('../auth.js');
const bcrypt = require('bcrypt');
const Product = require('../models/Product.js');
const Order = require('../models/Order.js');
const mongoose = require('mongoose');


// User registration w/ validation if email is already exists
module.exports.register_user = (request_body) => {
	return User.findOne({email: request_body.email}).then((result, error) => {
		if(result != null && result.email == request_body.email){
			return {message: `${request_body.email} is already in use, please provide different one`}
		} else {
	
	let new_user = new User ({
		firstName: request_body.firstName,
		lastName: request_body.lastName,
		age: request_body.age,
		mobileNo: request_body.mobileNo,
		email: request_body.email,
		password: bcrypt.hashSync(request_body.password, 10)
	});
	return new_user
	.save()
	.then((registered_user, error) => {
		if (error){
			return {message: error.message};
		}
		return {message: `You are successfully register, ${request_body.firstName}!`	};
			}).catch(error => console.log(error));
		}
	})
}
// User login
module.exports.login_user = (request, response) => {
	return User.findOne({email: request.body.email}).then(result => {

		if(result == null){
			return response.send({ message: "The user isn't register yet, please register first"})
		}
		const is_password_correct = bcrypt.compareSync(request.body.password, result.password);
		if(is_password_correct){
			return response.send({accessToken: auth.createAccessToken(result)});
		} else {
			return response.send({
				message: 'Password is incorrect.'
			})
		}
	}).catch(error => response.send(error))
}
// Retrieve user details
module.exports.retriveUserDetails = (request_body) => {
	return User.findOne({_id: request_body.id}).then((result, error) => {
		if(error){
			return {
				message: error.message
			}
		}
		result.password = "";
		return result;
	});
}

// to calculate the total amount
async function calculateTotalAmount(products) {
    let totalAmount = 0;

    for (const product of products) {
        const productDetails = await Product.findById(product.products.productId);
        if (productDetails) {
            const productTotal = productDetails.price * product.products.quantity;
            totalAmount += productTotal;
        }
    }

    return totalAmount;
}
// Order product 
module.exports.addToCart = async (request, response) => {
    if (request.user.isAdmin) {
        return response.send('Action Forbidden');
    }

    try {
        let user = await User.findById(request.user.id);
        if (!user) {
            return response.send({ message: 'User not found' });
        }

        let newOrder = {
            products: {
                productId: request.body.productId,
                productName: request.body.productName,
                quantity: request.body.quantity
            }
        };

        let totalAmount = await calculateTotalAmount([newOrder]);
        user.orderedProduct.push({
            products: newOrder.products,
            totalAmount: totalAmount
        });
        await user.save();

        let product = await Product.findById(request.body.productId);
        if (product) {
            product.userOrders.push({ userId: request.user.id });
            await product.save();
        }
        return response.send({ message: 'Product Successfully Added' });
    } catch (error) {
        return response.send({ message: error.message });
    }
};

module.exports.getOrders = (request, response) => {
	 User.findById(request.user.id)
	.then(user => response.send(user.orderedProduct))
	.catch(error => response.send(error.message))
}

// Delete Orders from user
module.exports.deleteOrder = (request, response) => {
	return User.findByIdAndUpdate(request.user.id, {$pull: {orderedProduct: {'products.productId': request.body.id}}}, {new: true}).then((result, error) =>{
	if (error) {
       return response.send({ message: 'Failed to delete order' });
      }
     
    if (!result) {
       return  response.send({ message: 'order not found' });
      } 
     return  response.send({ message: 'Order deleted successfully' });
    })
    .catch((error) => response.send({ message: 'Internal server error' })
    );
};

// to calculate the subtotal
function calculateSubtotal(orderedProducts) {
    let subtotal = 0;
    for (const orderedProduct of orderedProducts) {
        subtotal += orderedProduct.totalAmount;
    }
    return subtotal;
}
// Checkout order with subtotal
// With validation if 
module.exports.checkOut = (request, response) => {
    return User.findOne({ _id: request.body.id })
        .then((result, error) => {
        	console.log(request.user.id);
        	if(!mongoose.isValidObjectId(request.body.id)) {
                return response.send ({
                    message: 'User not found'
                });
            }
        	if(request.user.id != request.body.id){
        		return response.send({message: "Different User"});
        	}
        	
            if (error) {
                return response.send ({
                    message: error.message
                });
            }
            console.log(result)
            result.password = "";
            let subtotal = calculateSubtotal(result.orderedProduct);
            return response.send ({message: 
            `Hi Customer, ${result.firstName}. The Subtotal of your purchase is: ${subtotal}`});
            return response.send(subtotal);

            
        })
        .catch(error => {
            return {
                message: error.message
            };
        });
};

// // Checkout the product and total
// module.exports.checkOut = (request, response) => {
// 	return User.findById(request.user.id).then(result => {
// 		console.log(result)
// 	})
// }
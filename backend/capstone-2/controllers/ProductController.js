const auth = require('../auth.js')
const mongoose = require('mongoose')
const Product = require('../models/Product.js');

const mongooseID = mongoose.Types.ObjectId;

// Create product
module.exports.addProduct = (request, response) => {
	let new_product = new Product ({
		productName: request.body.productName,
		description: request.body.description,
		quantity: request.body.quantity,
		price: request.body.price
	});
	return new_product.save().then((added_product, error) => {
		if(error){
			return response.send(error.message);
		}
		return response.send("Successfully added a product")
	}).catch(error => console.log(error))
}

// Retrieve All Products
module.exports.getAllProduct = (request, response) => {
	return Product.find({}).then(result => {
		return response.send(result);
	})
}
// Retrieve All Active Product only
module.exports.getActiveProduct = (request, response) => {
	return Product.find({isActive: true}).then(result => {
		return response.send(result);
	})
}
// Retrieve Specific Product
module.exports.getSpecificProduct = (request, response) => {
	if(!mongooseID.isValid(request.params.id)){
		return response.send("Product not found")
	}
	return Product.findById(request.params.id).then(result => {
		return response.send(result);
	})
}
// Update Product
module.exports.updateProduct = (request, response) => {
	let update_product = {
		productName: request.body.productName,
		description: request.body.description,
		price: request.body.price,
		quantity: request.body.quantity
	};
	if(!mongooseID.isValid(request.params.id)){
		return response.send("Product is not available")
	}
	return Product.findByIdAndUpdate(request.params.id, update_product).then((product, error) => {
		if(error){
			return response.send(error.message)
		}
		return response.send('Updating the product is successfully!')
	})
}
// Product Archive
module.exports.archiveProduct = (request, response) => {
	let id = request.params.id;
	if(!mongooseID.isValid(request.params.id)){
		return response.send({message: "Product is not available"})
	}
	return Product.findByIdAndUpdate(id, {isActive: false}).then((result, error) => {
		if(!result.isActive){
		return response.send({message: `Product is already archived`})
	}
		
		if(error){
			return response.send({message: error.message})
			}
			return response.send({message: "Product is successfully archived"});
		}) 
}
// Product Re-activate
module.exports.activateProduct = (request, response) => {
	let id = request.params.id;
	
	if(!mongooseID.isValid(request.params.id)){
		return response.send({message: "Product is not available"})
	}
	return Product.findByIdAndUpdate(id, {isActive: true}).then((result, error) => {
	
	if(result.isActive){
		return response.send({message: `Product is already active`})
	}
		if(error){
			return response.send({message: error.message})
			}
			return response.send({message: "Product is successfully activate"});
		}) 
}
// Remove userId to userOrders
module.exports.removeUserToProduct = (request, response) => {
	let ProductID = request.params.id;
	let UserID = request.body.id;
	return Product.updateOne({_id: ProductID},{$pull: {userOrders: {userId: UserID}}}).then((result, error) =>{
	if (error) {
       return response.send({ message: 'Failed to delete UserID' });
      }
     
    if (!result) {
       return  response.send({ message: 'UserID not found' });
      } 
     return  response.send({ message: 'UserID deleted successfully' });
    })
    .catch((error) => response.send({ message: 'Internal server error' })
    );
};
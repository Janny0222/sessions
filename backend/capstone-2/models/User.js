const mongoose = require('mongoose')


const user_schema = new mongoose.Schema({
	firstName : {
		type: String,
		required: [true, "Please input first name"]
	},
	lastName : {
		type: String,
		required: [true, "Please input last name"]
	},
	age : {
		type: Number,
		required: [true, "Please input your age"]
	},
	mobileNo : {
		type: String,
		required: [true, "Please enter your mobile number"]
	},
	email : {
		type: String,
		required: [true, "Please input email"]
	},
	password : {
		type: String,
		required: [true, "Please input your password"]
	},
	isAdmin : {
		type: Boolean,
		default: false
	},
	orderedProduct: [
	{
		products: {
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			productName: {
				type: String,
				required: [true, "Product Name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			}
		},
		totalAmount: {
			type: Number,
		},
		purchaseOn: {
			type: Date,
			default: new Date()
		}
	}
	]
})



module.exports = mongoose.model('User', user_schema);
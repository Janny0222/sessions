const mongoose = require('mongoose');

const product_schema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, "Product Name is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	userOrders: [
	{
		userId: {
			type: String,
			required: [true, "User ID is required"]
		}
	}]
})

module.exports = mongoose.model("Product", product_schema);
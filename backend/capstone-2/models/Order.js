const mongoose = require('mongoose');


const order_schema = new mongoose.Schema({
	userId: {
		type: String,
		required: true
	},
	products: [
	{
		productId: {
			type: String,
			required: true
		},
		quantity: {
			type: Number,
			required: true
		},
	}
	],
	totalAmount: {
		type: Number,
		required: true
	},
	purchaseOn: {
		type: Date,
		default: new Date()
	}
})
const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserController.js')
const auth = require('../auth.js')

// Create User
router.post('/register', (request, response) => {
	UserController.register_user(request.body).then(result => {
		response.send(result)
	});
})
// Login
router.post('/login', (request, response) => {
	UserController.login_user(request, response);
})
router.post('/userProfile', (request, response) => {
	UserController.retriveUserDetails(request.body).then(result => {
		response.send(result)
	})
})

router.post('/order', auth.verify, (request, response) => {
	UserController.addToCart(request, response);
})

router.get('/checkOrder', auth.verify, (request, response) => {
	UserController.getOrders(request, response);
})

router.delete('/remove', auth.verify, (request, response) => {
	UserController.deleteOrder(request, response);
})

router.get('/checkout', auth.verify, (request, response) => {
	UserController.checkOut(request, response);
})

module.exports = router;
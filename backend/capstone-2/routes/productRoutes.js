const express = require('express');
const router = express.Router();
const ProductController = require('../controllers/ProductController.js');
const auth = require('../auth.js')

// Router to Add Product
router.post('/addProduct', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.addProduct(request, response)
})
// Router to get list of all Product
router.get('/allProduct', (request, response) => {
	ProductController.getAllProduct(request, response);
})
// Router to get list of active Product
router.get('/activeProduct', (request, response) => {
	ProductController.getActiveProduct(request, response);
})
// Router to get a single product
router.get('/:id', (request, response) => {
	ProductController.getSpecificProduct(request, response);
})
// Router to update a specific product
router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.updateProduct(request, response);
})
router.put('/:id/archive', auth.verify, auth.verifyAdmin, (request,response) => {
	ProductController.archiveProduct(request, response);
})
router.put('/:id/activate', auth.verify, auth.verifyAdmin, (request,response) => {
	ProductController.activateProduct(request, response);
})
router.delete('/:id/remove', auth.verify, (request, response) => {
	ProductController.removeUserToProduct(request, response);
})

module.exports = router;
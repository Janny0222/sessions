const jwt = require('jsonwebtoken');
const secret_key = "ECommerceAPI";

module.exports.createAccessToken = (user) => {
	const user_data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(user_data, secret_key, {});
}
module.exports.verify = (request, response, next) => {
let token = request.headers.authorization;

if(typeof token === "undefined"){
	return response.send('Authentication Failed, please make sure to include the token')
	}
	token = token.slice(7, token.length);
	jwt.verify(token, secret_key, (error, decoded_token) => {
		if(error){
			return response.send({
				auth: 'Failed', 
				message: error.message
			})
		}
		request.user = decoded_token;
		next();
	})
}
module.exports.verifyAdmin = (request, response, next) =>{
	if(request.user.isAdmin){
	return	next();
	}
	return response.send('Only the ADMIN can access this page')
}
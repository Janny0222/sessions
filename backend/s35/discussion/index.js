//Greater than operator

db.users.find({
    age: {
    $gt: 20
    }
})

db.users.find({
    age: {
    $gt: 70
    }
})

// Greater than or equal
db.users.find({
    age: {
    $gte: 82
    }
})

// Less than operator
db.users.find({
    age: {
    $lt: 80
    }
})

// Less than or equal operator
db.users.find({
    age: {
    $lte: 82
    }
})

// Regex operator
// output should be all firstname with letter 's' in it
db.users.find({
    firstName: {
        $regex: 's',
        $options: 'i'
    }
})

// Output should be all the lastname with letter 'a' in it
db.users.find({
    lastName: {
        $regex: 'a',
        $options: 'i'
    }
})

db.users.find({
    age: {
    $gt: 70
    },
    lastName: {
        $regex: 'g'
    }
})
db.users.find({
    age:{
        $lte: 76
    },
    firstName: {
        $regex: 'j',
        $options: 'i'
    }
})

// Field Projection
db.users.find({}, {
    "_id": 0
})

db.users.find({}, {
    "firstName": 1,
    "_id": 0
})
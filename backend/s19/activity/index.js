	let firstName = "Jan Michael";
	let lastName = "Villaruel";
	let age = 29;
	let hobbies = ["Watching Netflix", " Playing Online Games", " Workout"];
	let workAddress = {
		houseNumber: "32",
		street: "Washington",
		city: "Lincoln",
		state: "Nebraska"
	}
	console.log("First Name: " + firstName);
	console.log("Last Name: " + lastName);
	console.log("Age: " + age);
	console.log("Hobbies:");
	console.log(hobbies);
	console.log("Work Address:");
	console.log(workAddress);

	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ");
	console.log(profile);

	let fullName2 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName2);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);



	//DO NOT MODIFY
	//For exporting to test.js
	//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
	try{
		module.exports = {
		    firstName: typeof firstName !== 'undefined' ? firstName : null,
		    lastName: typeof lastName !== 'undefined' ? lastName : null,
		    age: typeof age !== 'undefined' ? age : null,
		    hobbies: typeof hobbies !== 'undefined' ? hobbies : null,
		    workAddress: typeof workAddress !== 'undefined' ? workAddress : null,
		    fullName: typeof fullName !== 'undefined' ? fullName : null,
		    currentAge: typeof currentAge !== 'undefined' ? currentAge : null,
		    friends: typeof friends !== 'undefined' ? friends : null,
		    profile: typeof profile !== 'undefined' ? profile : null,
		    fullName2: typeof fullName2 !== 'undefined' ? fullName2 : null,
		    lastLocation: typeof lastLocation !== 'undefined' ? lastLocation : null
		}
	} catch(err){
	}
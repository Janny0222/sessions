// alert("Hello World!");

console.log("Hello World!");

// [SECTION] Variables


// Varialbe Declartion and Invocation
let my_variable = "Hola, Mundo!";
console.log(my_variable);

// Concatenating Strings
let province = "Metro Manila"
let country = "Philippines"

let full_address = province + ", " + country
console.log(full_address);

// Numbers/Integers

let headcount = 26;
let grade = 98.7;

console.log("The number of Students is " + headcount + " and the Average of all Students is " + grade);

// let sum = headcount + grade;

// console.log(sum);

// Boolean - The Value of boolean is Only true or false
let	is_married = false;
let is_good_conduct = true;

console.log("He's Married: " + is_married);
console.log("She's a good person: " + is_good_conduct);

let grades = [98.7, 89.9, 90.2, 94.6];
let details = ["John", "Smith", 32, true];

console.log(details);


// Objects
let person = {
	fullName: "Juan Dela Cruz",
	age: 40,
	isMarried: false,
	contact: ["09123412342", "099922233334"],
	address: {
		houseNumber: "345",
		city: "England"
	}
}
console.log(person);

// Javascript read arrays as object. This is mainly to accomodate for specific functionalities that array can do later on.
console.log(typeof person);
console.log(typeof grades);

// Null and Undefined
let girlfriend = null;
let full_name;

console.log(girlfriend);
console.log(full_name);

// [SECTION] Operators

// Arithmetic Operators
let first_number = 5;
let second_number = 5;

let sum = first_number + second_number;
let difference = first_number - second_number;
let product = first_number * second_number
let quotient = first_number / second_number
let remainder = first_number % second_number;

console.log("Sum: " + sum);
console.log("Diffrence: " + difference);
console.log("Product: " + product);
console.log("Quotient: " + quotient);
console.log("Remainder: " + remainder);

// Assignment Operators
let assignment_number = 0;
assignment_number = assignment_number + 2;

console.log("Result of addition assignment operator: " + assignment_number);

assignment_number += 2;
console.log("Result of shorthand assignment operator: " + assignment_number);

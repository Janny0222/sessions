// using the aggregate method
db.fruits.aggregate([
    // $match - used to match or get documents that satisfies the conditon
    // $match is similar to find(). You can use query operators to make your criteria more flexible
    {$match: {onSale: true}},

    // $group - allows us to group together document and create an analysis output of the group elements
    {$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);

{
  _id: 1,
  total: 45
}
{
  _id: 2,
  total: 15
}




db.fruits.aggregate([
    // $match - Apple, Kiwi, Banana
    // $avg - gets the average of the values given field per group
{ $match: { onSale: true }},
    /*
    Apple - 1.0
    Kiwi - 1.0
    Banana - 2.0

    id: 1.0
        avgStocks: average stocks of fruits in 1.0
        avgStocks: (Apple stock + Kiwi stocks) / 2
        avgStocks: (20 + 25) / 2
        avgStocks: 22.5
    */
{ $group: {_id: '$supplier_id', avgStocks: { $avg: '$stock' }}},

    /*
    id: 2.0
        avgStocks: average stocks of fruits in 2.0
        avgStocks: Banana stocks / 1
        avgStocks: 15 / 1
        avgStocks: 15
    */

    // $project - can be use then aggregating data to include/exclude files from the returned results (field projection)
{ $project: {_id: 0 }}
]);

db.fruits.aggregate([
    // $match - Apple, Kiwi and Banana
  { $match: {onSale: true}},
    /*
    Apple: 1.0
    Kiwi: 1.0
    Banana: 2.0
    
    maxPrice: finds the highest price of fruit
    
    id 1.0
        maxPrice of Apple - 40
        maxPrice of Kiwi - 50

    id 2.0
        maxPrice of Banana - 25
    */
  { $group: {_id: '$supplier_id', maxPrice: {$max: '$price'}}},

  // $sort - to change the order of the aggregated result
  // Providing a value of -1 will sort the aggregated result in a reverse order
  { $sort: {maxPrice: -1}}
]);



db.fruits.aggregate([
    // $unwind - deconstruct the array field from a collection with an array value to output resuklt for each element
  {$unwind: '$origin'}
]);

db.fruits.aggregate([
  {$unwind: '$origin'},
  {$group: {_id: '$origin', kinds: {$sum: 1}}}
]);
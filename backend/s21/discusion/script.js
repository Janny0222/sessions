// FUNCTION DECLARTION
function printName(){
	console.log("My name is Jan");
}

// INVOCATION
printName();

// FUNCTION EXPRESSION
let variable_function = function(){
	console.log("Hello from function expression");
};

// Invoke the variable name instead a function
variable_function();

// SCOPING
let global_variable = "Call me Mr. WorldWide";

console.log(global_variable);

function showName(){
	let function_variable = "Joe";
	const function_const = "John";

	console.log(function_variable);
	console.log(function_const);
	// you can use global variable inside any function as long as they are declared outside the function
	console.log(global_variable);
}
// You cannot use local scope variables outside the function they are declared in
// console.log(function_variable);
showName();

// NESTED FUNCTION
function parentFunction(){
	let name = "Jane";

	function childFunction(){
		let nested_name = "John";
		// Parent variable
		console.log(name);

		// child variable
		console.log(nested_name);
	}
	
	childFunction();
	
	// console.log(nested_name); -> Encounter error due to nested_name not define under parentFunction
}
parentFunction();

// BEST PRACTIC FOR FUNCTION NAMING
// function printWelcomeMessageForUser(){
// 	// Prompt input box
// 	let first_name = prompt("Enter Your First Name");
// 	let last_name = prompt("Enter Your Last Name");

// 	console.log("Hello, " + first_name + " " + last_name + "!");
// 	console.log("Welcome sa page ko!");
// }
// printWelcomeMessageForUser();

// RETURN STATEMENT
function fullName(){

	// set  the value of function
	return "Jan Villaruel";
}
// need to console.log to view the value of function
console.log(fullName());

let obj = {
	name: 'Janny',
	age: 28
}

import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// Importing Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

// This is where React attaches the component to the root element in the index.html file.
// StrictMode allows react to be able to display and handle any errors/warning that may occur
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

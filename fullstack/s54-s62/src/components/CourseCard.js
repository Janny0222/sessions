import {Button, Card} from 'react-bootstrap';
import PropTypes from 'prop-types'; // To validate the props
import {useState} from 'react';

	// using {course} changing the props for desctructuring
export default function CourseCard({course}){

	// destructure the content of 'course'
	const {name, description, price} = course;

	// A state is just like a variable but with the concept of getters and setters. The getter is responsible for retrieving the current value of the state, while the setter is responsible for modifying the current value of the state. The useState() hook i responsible for setting the initial value of the state.
	const [count, setCount] = useState(0);
	const [seat, setSeat] = useState(30);

	function enroll(){
		// The setCount function can use the previous value of the state and add/modify to it.
		if(count < 30){
			setCount(prev_value => prev_value + 1);
		} else {
			alert("Reach the maximum enrollee's");
		}

	}
	function seats(){
		if(seat > 0){
			setSeat(seat - 1);
		} else {
			alert('No more seats available')
		}

	}
	let enrollSeats = () => {
		enroll();
		seats();
	}

	return (
		<Card className="my-2">
			<Card.Body >
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
						
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PhP {price}</Card.Text>

				<Card.Subtitle>Enrollees:</Card.Subtitle>
				<Card.Text>{count}</Card.Text>
				<Card.Subtitle>Available Seats:</Card.Subtitle>
				<Card.Text>{seat}</Card.Text>
				<Button variant="primary" onClick={enrollSeats}>Enroll</Button>
			</Card.Body>
		</Card>

	)

}
	
// PropTypes is used for validating the data from the props
CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}